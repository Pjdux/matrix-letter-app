import React from "react";
export const AgeOptions = Array.from(Array(90), (_, x) => {
  return { label: x + 1, value: x + 1 };
}).slice(19);

// /console.log("this are the AgeOptions", AgeOptions);
