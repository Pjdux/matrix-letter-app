import styled from "styled-components";

const ContactWrapper = styled.div`
  textarea {
    height: 250px;
    width: 90%;
    max-width: 300px;
  }
  label {
    color: #2f1728;
    font-weight: 300;
    text-decoration: underline #ffb612;

    padding: 5px;
    font-family: "M PLUS Rounded 1c", sans-serif;
  }

  input {
    width: 90%;
    max-width: 300px;
    padding: 10px;
  }
  button {
    color: #09487b;
    margin-top: 10px;
  }
`;

export default ContactWrapper;
